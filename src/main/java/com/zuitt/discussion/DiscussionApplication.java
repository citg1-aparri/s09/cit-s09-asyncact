package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.ArrayList;

@SpringBootApplication
@RestController
@RequestMapping("/greeting")
public class DiscussionApplication {
	ArrayList<String> enrollees = new ArrayList<String>();
	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}

	@GetMapping("/hello")
	public String hello(){
		return "Hello World";
	}

	//localhost:8080/hi?name=value
	@GetMapping("/hi")
	public String hi(@RequestParam(value="name", defaultValue="John") String name){
		return String.format("Hi %s", name);
	}
	//localhost:8080/friend?name=value&friend=value
	@GetMapping("/friend")
	public String friend(@RequestParam(value="name", defaultValue="Joe") String name, @RequestParam(value="friend", defaultValue="Jane") String friend){
		return String.format("Hello %s! My Name is %s.", friend, name);
	}

	@GetMapping("/hello/{name}")
	public  String greetFriend(@PathVariable("name")String name){
		return String.format("Nice to meet you, %s!", name);
	}

	// Activity for s09
	@GetMapping("/enroll")
	public String enroll(@RequestParam(value = "user") String user){
		enrollees.add(user);
		return String.format("Thank you for enrolling, %s!", user);
	}

	@GetMapping("/getEnrollees")
	public String getEnrollees(){
		return enrollees.toString();
	}

	@GetMapping("/nameage")
	public String nameage(@RequestParam(value="name") String name, @RequestParam(value="age")int age){
		return String.format("Hello %s! My age is %d.", name, age);
	}

	@GetMapping("/courses/{id}")
	public String courses(@PathVariable(value="id") String id){
		if(id.equals("java101")){
			return "Name: Java 101, Schedule: MWF 8:00 AM - 11:00 AM, Price: PHP 3000.00";
		}else if(id.equals("sql101")){
			return "Name: SQL 101, Schedule: TTH 1:00 PM - 4:00 PM, Price: PHP 2000.00";
		}else if(id.equals("javaee101")){
			return "Name: Java EE 101, Schedule: MWF 1:00 PM - 4:00 PM, Price: PHP 3500.00";
		}else{
			return "Course cannot be found";
		}
	}

    //Async Activity
    @GetMapping("/welcome")
    public String welcome(@RequestParam(value="user") String user, @RequestParam(value="role") String role){
        if(role.equals("admin")){
            return String.format("Welcome back to the class portal, Admin %s!", user);
        }else if(role.equals("teacher")){
            return String.format("Thank you for logging in, Teacher %s!", user);
        }else if(role.equals("student")){
            return String.format("Welcome to the class portal, %s!", user);
        }else{
            return "Role out of range!";
        }
    }
	ArrayList<Student> students = new ArrayList<Student>();
    @GetMapping("/register")
    public String register(@RequestParam(value="id") int id, @RequestParam(value="name") String name,
						   @RequestParam(value="course") String course){
		Student student = new Student(id, name, course);
		students.add(student);
		return String.format("%d your id number is registered on the system!", id);
    }

	@GetMapping("/account/{id}")
	public String getAccount(@PathVariable(value = "id") int id){
		for(Student s: students){
			if(s.getId() == id){
				return String.format("Welcome back %s! You are currently enrolled in %s", s.getName(), s.getCourse());
			}
		}
		return String.format("Your provided %d is not found in the system!", id);
	}




}
