package com.zuitt.discussion;

public class Student {
        public int id;
        public String name;
        public String course;
        public Student(){
        }
        public Student(int id, String name, String course){
            this.id = id;
            this.name = name;
            this.course = course;
        }

        public int getId(){
            return id;
        }

        public String getName() {
            return name;
        }

        public String getCourse() {
            return course;
        }
}
